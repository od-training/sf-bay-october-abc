import { Component, OnInit } from '@angular/core';
import { Video } from '../types';
import { HttpClient } from '@angular/common/http';
import { VideoDataService } from 'src/app/video-data.service';
import { Observable } from 'rxjs';

const apiUrl = 'https://api.angularbootcamp.com/';

@Component({
  selector: 'app-video-dashboard',
  templateUrl: './video-dashboard.component.html',
  styleUrls: ['./video-dashboard.component.css']
})
export class VideoDashboardComponent implements OnInit {
  videos: Observable<Video[]>;
  selectedVideo: Video | undefined;

  constructor(svc: VideoDataService) {
    this.videos = svc.loadVideos();
  }

  ngOnInit() {}

  setVideo(video: Video) {
    this.selectedVideo = video;
  }
}
