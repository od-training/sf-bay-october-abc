import { Component } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-stat-filters',
  templateUrl: './stat-filters.component.html',
  styleUrls: ['./stat-filters.component.css']
})
export class StatFiltersComponent {
  filters: FormGroup;

  constructor(fb: FormBuilder) {
    this.filters = fb.group({
      name: [''],
      title: ['']
    });
  }

  logTheForm() {
    console.log(this.filters.value);
  }
}
