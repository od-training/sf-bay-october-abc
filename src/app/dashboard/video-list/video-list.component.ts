import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Video } from '../types';

@Component({
  selector: 'app-video-list',
  templateUrl: './video-list.component.html',
  styleUrls: ['./video-list.component.css']
})
export class VideoListComponent implements OnInit {
  @Input() videoList: Video[];
  @Output() setSelectedVideo = new EventEmitter<Video>();
  selectedVideo: Video | undefined;

  constructor() {}

  ngOnInit() {}

  selectVideo(video: Video) {
    this.selectedVideo = video;
    this.setSelectedVideo.emit(video);
  }
}
